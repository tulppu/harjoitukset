/*
    Lomittaisjärjeytyksen ja puolitushaun toteutus merkkijonoille C:llä.
*/


#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct Sana
{
	char* sana;
	size_t pituus;

	void *edellinen;
	void *seuraava;
};

short unsigned onko_pienempi(const struct Sana* sana1, const struct Sana* sana2)
{
	char* s1 = sana1->sana;
	char* s2 = sana2->sana;

	// strcmp järjestää ääkköset epätoivotulla tavalla.
	while (1)
	{
		if (s1[0] != s2[0]) return ((size_t) s1[0] < (size_t) s2[0]) ? 1 : 0;
		if (s1[0] == '\0') return 1;

		s1++;
		s2++;
	}
}

short unsigned onko_pienempi_kirjain(const char kirjain1, const char kirjain2)
{
	return (size_t) kirjain1 < (size_t) kirjain2;
}

int hae_pienin_indeksi(struct Sana **sanat, unsigned int koko,
				unsigned int alku)
{
	size_t pienin_indeksi = alku;
	struct Sana* pienin = sanat[alku];

	for (int i = alku; i < koko; i++)
	{
		if (onko_pienempi(sanat[i], pienin))
		{
			pienin_indeksi = i;
			pienin = sanat[pienin_indeksi];
		}
	}

	return pienin_indeksi;
}

int hae_pienin_indeksi_kirjaimella(struct Sana **sanat, unsigned int koko,
					unsigned int alku, size_t syvyys)
{
	size_t pienin_indeksi = alku;
	struct Sana* pienin = sanat[alku];

	struct Sana* hetkinen = NULL;

	for (int i = alku; i < koko; i++)
	{
		hetkinen = sanat[i];

		if (syvyys && strncmp(hetkinen->sana, pienin->sana, syvyys) != 0) break;

		if ( (size_t) hetkinen->sana[syvyys] < (size_t) pienin->sana[syvyys] )
		{
			pienin_indeksi = i;
			pienin = hetkinen;
		}
	}

	return pienin_indeksi;
}

void vaihda_arvot(struct Sana** sanat, unsigned int indeksi1, unsigned int indeksi2)
{
	struct Sana* hetkinen = sanat[indeksi1];

	sanat[indeksi1] = sanat[indeksi2];
	sanat[indeksi2] = hetkinen;
}

/*
void valintajärjestä(struct Sana** sanat, unsigned int koko)
{
	for (size_t i = 0; i < koko; i++)
	{
		const int pienin = hae_pienin_indeksi(sanat, koko, i);
		vaihda_arvot(sanat, i, pienin);
	}
}

// järjestää listan syvyys kerrallaan
void valintajärjestä2(struct Sana** sanat, unsigned int koko)
{
	size_t pisin = 0;
	for (size_t i = 0; i < koko; i++)
	{
		if (sanat[i]->pituus > pisin)
		{
			pisin = sanat[i]->pituus;
		}
	}

	for (size_t syvyys = 0; syvyys < pisin; syvyys++)
	{
		printf("Syvyys: %ld\n", syvyys);
		for (size_t i = 0; i < koko; i++)
		{
			//if (sana->pituus < syvyys) continue;

			const int pienin = hae_pienin_indeksi_kirjaimella(sanat, koko, i, syvyys);
			vaihda_arvot(sanat, i, pienin);
		}
	}
}*/

void yhdista
(struct Sana** sanat, const unsigned int alku, const unsigned int keski, const unsigned int loppu)
{
	struct Sana* hetkinen[loppu - alku + 1];

	size_t i = alku, j = keski + 1;
	size_t k = 0; // viimeksi lisätty indeksi hetkiseen

	while(i <= keski && j <= loppu)
	{
		if (onko_pienempi(sanat[i], sanat[j]))
		//if ((size_t) sanat[i]->sana[0] <= (size_t) sanat[j]->sana[0])
		{
			hetkinen[k] = sanat[i];
			i++;
		}
		else
		{
			hetkinen[k] = sanat[j];
			j++;
		}
		k++;
	}

	while (i <= keski)
	{
		hetkinen[k] = sanat[i];
		k++;
		i++;
	}

	while (j <= loppu)
	{

		hetkinen[k] = sanat[j];
		k++;
		j++;
	}

	for (i = alku; i <= loppu; i++) {
		sanat[i] = hetkinen[i - alku];
	}

	return;

}

void lomittaisjärjestä(struct Sana** sanat, unsigned int alku, unsigned int loppu)
{
	if (alku >= loppu) return;

	const size_t keskikohta = (alku + loppu) / 2;

	lomittaisjärjestä(sanat, alku, keskikohta);
	lomittaisjärjestä(sanat, keskikohta + 1, loppu);

	yhdista(sanat, alku, keskikohta, loppu);
}

struct puolitushaku
{
	struct Sana** sanat;
	unsigned int koko;

	//char haettu_arvo[254];
	struct Sana haettu_sana;

	unsigned int edellinen_jakokohta;

	unsigned int pienin_mahdollinen;
	unsigned int suurin_mahdollinen;

	unsigned long long hypyt;

	long long löytyinyt_indeksi;
	char *tulos;
};

struct puolitushaku ph_luo_haku(struct Sana** sanat, unsigned int koko, char *haettu_sana)
{
	struct puolitushaku haku;
	haku.sanat = sanat;
	haku.koko = koko;

	struct Sana sana;

	sana.seuraava = NULL;
	sana.edellinen = NULL;

	sana.pituus = strlen(haettu_sana);
	sana.sana = malloc(sana.pituus + 1);
	strcpy(sana.sana, haettu_sana);

	haku.haettu_sana = sana;

	haku.edellinen_jakokohta = -1;

	haku.pienin_mahdollinen = 0;
	haku.suurin_mahdollinen = koko;

	haku.löytyinyt_indeksi = -1;
	haku.tulos = NULL;

	haku.hypyt = 0;

	return haku;
}

unsigned ph_hae_indeksi(struct puolitushaku* haku)
{
	while (haku->pienin_mahdollinen < haku->suurin_mahdollinen && haku->löytyinyt_indeksi == -1)
	{
		unsigned int jakokohta = haku->pienin_mahdollinen + ((haku->suurin_mahdollinen - haku->pienin_mahdollinen) / 2);

		if (jakokohta == haku->edellinen_jakokohta) break;

		if (strcmp(haku->sanat[jakokohta]->sana, haku->haettu_sana.sana) == 0) {
			haku->löytyinyt_indeksi = jakokohta;
			haku->tulos = haku->sanat[jakokohta]->sana;
			break;
		}
		if (onko_pienempi(haku->sanat[jakokohta], &haku->haettu_sana))
		{
			haku->pienin_mahdollinen = jakokohta;
		}
		else
		{
			haku->suurin_mahdollinen = jakokohta;
		}
		haku->edellinen_jakokohta = jakokohta;
		haku->hypyt++;

	}
	return haku->löytyinyt_indeksi !=-1;
}


struct Sana** lataa_sanat(size_t* sanamäärä)
{
	unsigned long long varattu_sanamäärä = 128;

	struct Sana** sanat = malloc(varattu_sanamäärä * sizeof(struct Sana*));

	FILE *sanatiedosto = fopen("sanat.txt", "r");
	if (!sanatiedosto)
	{
		printf("'sanat.txt' -tiedoston luku epäonnistui\n");
		return 0;
	}

	char sana[128];
	while (!feof(sanatiedosto))
	{
		if (*sanamäärä == varattu_sanamäärä)
		{
			varattu_sanamäärä *= 2;
			sanat = (struct Sana**) realloc(sanat, varattu_sanamäärä * sizeof(struct Sana*));
		}

		fgets(sana, 127, sanatiedosto);
		const size_t pituus = strlen(sana);

		if (sana == NULL || pituus <= 1) continue;
		sana[pituus-1] = '\0'; // '\n' pois lopusta

		sanat[*sanamäärä] = malloc(sizeof(struct Sana));
		sanat[*sanamäärä]->sana = malloc(pituus);
		strcpy(sanat[*sanamäärä]->sana, sana);
		sanat[*sanamäärä]->pituus = pituus-1;

		if (*sanamäärä)
		{
			sanat[*sanamäärä-1]->seuraava = sanat[*sanamäärä];
			sanat[*sanamäärä]->edellinen = sanat[*sanamäärä-1];
		}

		(*sanamäärä)++;
	}
	fclose(sanatiedosto);

	return sanat;
}


int main(int lahetemäärä, char *lahetteet[])
{
	char hakusana[254];
	if (lahetemäärä > 1) strcpy(hakusana, lahetteet[1]);
	else strcpy(hakusana, "kala");

	size_t sanamäärä = 0;
	struct Sana** sanat = lataa_sanat(&sanamäärä);

	printf("Lisättiin %ld sanaa\n", sanamäärä);
	lomittaisjärjestä(sanat, 0, sanamäärä - 1);
	printf("Järjestettiin\n");

	struct puolitushaku haku = ph_luo_haku(sanat, sanamäärä, hakusana);
	const unsigned löytyi = ph_hae_indeksi(&haku);

	if (löytyi)
	{
		printf("%lld:n hypyn jälkeen haku '%s' löytyi indeksistä %lld\n\n",
			haku.hypyt, haku.haettu_sana.sana,  haku.löytyinyt_indeksi);

		const size_t alku = (haku.löytyinyt_indeksi - 15 < 0) ? 0 : haku.löytyinyt_indeksi - 15;
		for (size_t i = alku;  i < sanamäärä && i < haku.löytyinyt_indeksi + 15; i++)
		{
			printf("%ld: %s\n", i, sanat[i]->sana);
		}
	}
	else
	{
		printf("Hakusanaa '%s' ei löytynyt\n", hakusana);
	}
	return 0;
}
