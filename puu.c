/*
    Punamustan binääripuun toteutus C:llä, poisto-operaatio puuttuu.
*/

#define _POSIX_C_SOURCE 199309L

#include <stdio.h>
#include <unistd.h>
#include <bits/time.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <stdio.h>
#include <stdint.h>
#include <sys/mman.h>
#include <unistd.h>

#define KEHITYSTILA 0
#ifdef KEHITYSTILA
 #define KEHITYSTULOSTIN(...) if (KEHITYSTILA) printf(__VA_ARGS__)
#else
 #define KEHITYSTULOSTIN(...)
#endif

#define MONTAKO 1024 * 1024
//##define SOLMUJA_PER_SIVU 1024 * 1024

#define likely(x)      __builtin_expect(!!(x), 1)
#define unlikely(x)    __builtin_expect(!!(x), 0)

#include <stdint.h>

enum väri
{
	PUNAINEN = 0,
	MUSTA = 1
};

struct Solmu
{
	short avain;
	struct Solmu* vasen;
	struct Solmu* oikea;
	enum väri väri;
	struct Solmu* isäntä;
	void* data;
};



int solmuja_per_sivu()
{
	static int sivukoko = 0;
	if (!sivukoko) sivukoko = sysconf(_SC_PAGESIZE);
	return (sivukoko * 512) / sizeof(struct Solmu);
}


struct Puu
{
	struct Solmu *juuri;
	size_t koko;

	// Käytössä oleva sivu
	void *sivu;
	void **sivut;

	// Linkitetty lista (oikealle) vapautetuista solmuista uusiokäyttöä varten.
	struct Solmu *vapautetut_solmut;

	size_t sivut_n;
	size_t sivu_nyt;
	size_t sivuja_varattu;
	size_t solmu_i;
};

// Luo puun ja alustaa siihen alkioita/solmuja varten ensimmäiseen sivuun muistia solmuja_per_sivu() määrän verran.
struct Puu* luo_puu()
{
	struct Puu *puu = malloc(sizeof(struct Puu));

	puu->juuri = NULL;
	puu->koko = 0;
	puu->sivu_nyt = 0;
	puu->solmu_i = 0;


	puu->vapautetut_solmut = NULL;

	puu->sivut = malloc(1 * sizeof(void*));
	//puu->sivut[0] = malloc(solmuja_per_sivu() * sizeof(struct Solmu));
	puu->sivut[0] = mmap(NULL, sizeof(struct Solmu) * solmuja_per_sivu(), PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
	assert(puu->sivut[0]);

	puu->sivu = puu->sivut[0];

	return puu;
}

void vapauta_puu(struct Puu* puu)
{
	for (uint16_t i = 0; i < puu->sivu_nyt + 1; i++)
	{
		//free(puu->sivut[i]);
		munmap(puu->sivut[i], solmuja_per_sivu() * sizeof(struct Solmu));
	}
	free(puu->sivut);
	free(puu);
	puu = NULL;
}


// Varaa sivusta solmun ja allokoi puuhun tarvittaessa uuden sivun.
// Vika: Jos sivu tyhjenee, ei sitä vapauteta.
inline static struct Solmu* varaa_solmu(struct Puu* puu)
{
	if (puu->vapautetut_solmut)
	{
		struct Solmu* solmu = puu->vapautetut_solmut;
		puu->vapautetut_solmut = solmu->oikea;

		solmu->vasen = NULL;
		solmu->oikea = NULL;
		solmu->isäntä = NULL;
		solmu->väri = PUNAINEN;

		return solmu;
	}

	// Sivu täynnä, luodaan uusi.
	if (puu->solmu_i == solmuja_per_sivu())
	{
		puu->sivu_nyt++;
		KEHITYSTULOSTIN("Varataan uusi sivu %ld\n", puu->sivu_nyt);

		puu->sivut = realloc(puu->sivut, (puu->sivu_nyt + 1) * sizeof(void*));

		if (puu->sivut == NULL)
		{
			printf("Muistivirhe(loppu?)\n");
			exit(EXIT_FAILURE);
		}
		//printf("Varataan puu\n");
		puu->sivut[puu->sivu_nyt] = mmap(NULL, sizeof(struct Solmu) * solmuja_per_sivu(), PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
		assert(puu->sivut[puu->sivu_nyt]);
		//puu->sivut[puu->sivu_nyt] = malloc(sizeof(struct Solmu) * solmuja_per_sivu());
		puu->sivu = puu->sivut[puu->sivu_nyt];

		puu->solmu_i = 0;
	}
	struct Solmu *solmu = puu->sivu + puu->solmu_i * sizeof(struct Solmu);
	//struct Solmu *solmu = malloc(sizeof(struct Solmu));
	puu->solmu_i++;

	solmu->vasen = NULL;
	solmu->oikea = NULL;
	solmu->isäntä = NULL;
	solmu->väri = PUNAINEN;

	return solmu;
}


inline  struct Solmu* hae(struct Solmu* solmu,  long long avain)
{
	while(solmu)
	{
		if (!solmu) break;
		if (avain < solmu->avain)
		{
			solmu = solmu->vasen;
			continue;
		}
		else if (avain > solmu->avain)
		{
			solmu = solmu->oikea;
			continue;
		}
		else
		{
			return solmu;
		}
	}
	return NULL;
}

inline int sisältääkö(struct Solmu* solmu,  long long avain)
{
	while(solmu)
	{
		if (avain == solmu->avain) return 1;
		if (avain < solmu->avain) solmu = solmu->vasen;
		else solmu = solmu->oikea;
	}
	return 0;
}

inline static struct Solmu* hae_puusta(struct Puu* puu, long long avain)
{
	return hae(puu->juuri, avain);
}
struct Solmu* hae_suurin(struct Solmu* solmu)
{
	while (solmu)
	{
		if (solmu->oikea) solmu = solmu->oikea;
		else break;
	}
	return solmu;
}

struct Solmu* hae_pienin(struct Solmu* solmu)
{
	while (solmu)
	{
		if (solmu->vasen) solmu = solmu->vasen;
		else break;
	}
	return solmu;
}

short int hakuaika(struct Solmu* solmu, long long avain)
{
	clock_t tik = clock();
	struct Solmu *löytyi = hae(solmu, avain);
	clock_t tok = clock();

	if (löytyi) printf("Avain %lld löytyi\n", avain);
	else printf("Avainta %lld ei lötynyt\n", avain);

	printf("Hakuaikaa kului %f sekunttia\n\n", (double)(tok - tik) / CLOCKS_PER_SEC);

	return löytyi != NULL;
}


inline static struct Solmu* hae_sisar(struct Solmu* solmu)
{
	struct Solmu* isäntä = solmu->isäntä;
	if (!isäntä) return NULL;
	return (isäntä->vasen == solmu) ? isäntä->oikea : isäntä->vasen;
}

inline static struct Solmu* hae_setä(struct Solmu* solmu)
{
	struct Solmu* isäntä = solmu->isäntä;
	#ifdef KEHITYSTILA
	assert(isäntä != NULL);
	#endif
	return hae_sisar(isäntä);
}


void tarkista_solmu(struct Solmu* solmu)
{
	// KEHITYSTULOSTIN("Solmu %lld\n", solmu->avain);
	assert(solmu != solmu->isäntä);
	assert(solmu != solmu->vasen);
	assert(solmu != solmu->oikea);

	if (solmu->vasen || solmu->oikea) assert(solmu->vasen != solmu->oikea);

	if (solmu->vasen) assert(solmu->vasen->avain < solmu->avain);
	if (solmu->oikea) assert(solmu->oikea->avain >= solmu->avain);

	if (solmu->isäntä)
	{
		assert(solmu->isäntä != solmu->oikea);
		assert(solmu->isäntä != solmu->vasen);
		assert(solmu->isäntä->vasen == solmu || solmu->isäntä->oikea == solmu);
		assert(solmu->isäntä->isäntä != solmu);
	}


	if (solmu->vasen) assert(solmu->vasen->isäntä == solmu);
	if (solmu->oikea) assert(solmu->oikea->isäntä == solmu);


	if (solmu->väri == PUNAINEN)
	{
		assert(solmu->isäntä);
		assert(solmu->isäntä->väri == MUSTA);

		if (solmu->vasen) assert(solmu->vasen->väri == MUSTA);
		if (solmu->oikea) assert(solmu->oikea->väri == MUSTA);
	}
}


unsigned long long korkeus(struct Solmu* puu)
{
	unsigned long long vasen = (puu && puu->vasen) ? 1 : 0;
	unsigned long long oikea = (puu && puu->oikea) ? 1 : 0;

	if (vasen) vasen += korkeus(puu->vasen);
	if (oikea) oikea += korkeus(puu->oikea);

	//printf("%lld\n", vasen);

	return (vasen > oikea) ? vasen : oikea;
}

unsigned long long jälkeläismäärä(struct Solmu* puu)
{
	if (puu == NULL) return 0;

	unsigned long long määrä = 1;
	määrä += jälkeläismäärä(puu->oikea);
	määrä += jälkeläismäärä(puu->vasen);

	return määrä;
}

void tarkista_puu(struct Solmu* puu)
{
	tarkista_solmu(puu);
	if (puu->oikea) tarkista_puu(puu->oikea);
	if (puu->vasen) tarkista_puu(puu->vasen);
}

inline static struct Solmu* tasapainota(struct Solmu* solmu);

inline static void värinvaihto(struct Solmu* isä)
{
	struct Solmu* setä = hae_sisar(isä);
	#if KEHITYSTILA
	assert(setä);
	#endif

	isä->väri = setä->väri = MUSTA;

	struct Solmu* isoisä = isä->isäntä;

	#if KEHITYSTILA
	assert(isoisä);
	assert(isoisä->väri == MUSTA);
	#endif

	// ei vaihdeta alkujuuren väriä
	if (isoisä->isäntä) isoisä->väri = PUNAINEN;
}

inline static struct Solmu* tasapainota(struct Solmu* solmu)
{
	//return solmu;
	struct Solmu* puu = solmu->isäntä;
	if (!puu) return solmu;

	struct Solmu* isoisä = puu->isäntä;

	// ei tarvetta
	if (puu->väri == MUSTA)
	{
		KEHITYSTULOSTIN("Ei tasoitusta\n");
		return solmu;
	}

	if (puu->väri == PUNAINEN)
	{
		struct Solmu* setä = hae_sisar(puu);
		// värinvaihto riittää
		if (setä && setä->väri == PUNAINEN)
		{
			KEHITYSTULOSTIN("Tehdään värinvaihto..\n");
			värinvaihto(puu);

			if (isoisä->isäntä && isoisä->isäntä->väri == PUNAINEN)
			{
				KEHITYSTULOSTIN("punikki isoisoisä\n");
				return tasapainota(isoisä);
			}

			KEHITYSTULOSTIN("..tehty\n");
			return solmu;
		}
	}

	unsigned yksinkertainen = (
					(isoisä->vasen && isoisä->vasen->vasen == solmu)
					|| (isoisä->oikea && isoisä->oikea->oikea == solmu)
				);
	// juuresta uusi musta isoisä
	// isoisä juuren punaiseksi vasemmaksi
	// solmu valmiiksi oikealla paikalla
	if (yksinkertainen)
	{
		KEHITYSTULOSTIN("Yksinkertainen kierto..\n");

		struct Solmu* lapsi_vasen = puu->vasen;
		struct Solmu* lapsi_oikea = puu->oikea;

		puu->väri = MUSTA;

		const unsigned suuntana_vasen = solmu->avain < puu->avain;

		struct Solmu* isoisoisä = (isoisä) ? isoisä->isäntä : NULL;

		if (suuntana_vasen)
		{
			isoisä->vasen = NULL; // osoitti juureen
			if (lapsi_oikea)
			{
				isoisä->vasen = lapsi_oikea;
				isoisä->vasen->isäntä = isoisä;
			}
			puu->oikea = isoisä;
		}
		else
		{
			isoisä->oikea = NULL; // osoitti juureen
			if (lapsi_vasen)
			{
				isoisä->oikea = lapsi_vasen;
				isoisä->oikea->isäntä = isoisä;
			}
			puu->vasen = isoisä;
		}
		if (puu->vasen)
		{
			puu->vasen->isäntä = puu;
			puu->vasen->väri = PUNAINEN;
		}
		if (puu->oikea)
		{
			puu->oikea->isäntä = puu;
			puu->oikea->väri = PUNAINEN;
		}
		//puu->vasen->isäntä = puu->oikea->isäntä = puu;
		//puu->vasen->väri = puu->oikea->väri = PUNAINEN;

		puu->isäntä = isoisoisä;
		if (puu->isäntä)
		{
			#if KEHITYSTILA
			assert(puu->isäntä->vasen == isoisä || puu->isäntä->oikea == isoisä);
			#endif
			if (puu->isäntä->vasen == isoisä) puu->isäntä->vasen = puu;
			else puu->isäntä->oikea = puu;
		}

		#if KEHITYSTILA
		assert(puu->oikea->isäntä == puu && puu->vasen->isäntä == puu);
		assert(puu != puu->isäntä);
		#endif

		KEHITYSTULOSTIN("..tehty\n");
		return puu;
		//return tasapainota(puu);
	}
	// solmusta uusi musta isoisä
	// solmun vasemmalle ex-isä punaisena
	// solmun oikeaksi ex-isoisä punaisena
	else // kaksinkertainen (vasen + oikea)
	{
		KEHITYSTULOSTIN("Kaksinkertainen kierto..\n");

		solmu->isäntä = isoisä->isäntä;
		solmu->väri = MUSTA;

		const unsigned suuntana_oikea = isoisä->vasen && isoisä->vasen->oikea == solmu;

		struct Solmu* lapsi_vasen = solmu->vasen;
		struct Solmu* lapsi_oikea = solmu->oikea;

		if (solmu->isäntä)
		{
			unsigned onko_vasen = isoisä->isäntä->vasen == isoisä;

			#if KEHITYSTILA
			unsigned onko_oikea = isoisä->isäntä->oikea == isoisä;
			assert(onko_vasen || onko_oikea);
			#endif

			if (onko_vasen) solmu->isäntä->vasen = solmu;
			else solmu->isäntä->oikea = solmu;
		}

		if (suuntana_oikea)
		{
			solmu->oikea = isoisä;
			solmu->oikea->vasen = NULL; // osoittin juureen
			solmu->vasen = puu;
			solmu->vasen->oikea = NULL; // osoitti solmuun
		}
		else
		{
			solmu->oikea = puu;
			solmu->oikea->vasen = NULL; // osoitti solmuun
			solmu->vasen = isoisä;
			solmu->vasen->oikea = NULL; // osoitti juureen
		}
		solmu->vasen->isäntä = solmu->oikea->isäntä = solmu;
		solmu->vasen->väri = solmu->oikea->väri = PUNAINEN;
		if (suuntana_oikea)
		{
			if (lapsi_vasen)
			{
				puu->oikea = lapsi_vasen;
				lapsi_vasen->isäntä = puu;
			}
			if (lapsi_oikea)
			{
				isoisä->vasen = lapsi_oikea;
				lapsi_oikea->isäntä = isoisä;
			}
		}
		else
		{
			if (lapsi_vasen)
			{
				puu->vasen = lapsi_oikea;
				lapsi_oikea->isäntä = puu;
			}
			if (lapsi_oikea)
			{
				isoisä->oikea = lapsi_vasen;
				lapsi_vasen->isäntä = isoisä;
			}
		}

		#if KEHITYSTILA
		assert(solmu->vasen->isäntä == solmu && solmu->oikea->isäntä == solmu);
		#endif

		KEHITYSTULOSTIN("..tehty\n");
		return solmu;
	}

}

inline static struct Solmu* lisää(struct Solmu* puu, struct Solmu *uusi_solmu)
{
	uusi_solmu->väri = PUNAINEN;
	//uusi_solmu->vasen = NULL;
	//uusi_solmu->oikea = NULL;

	while(1)
	{
		if (uusi_solmu->avain < puu->avain)
		{
			if (puu->vasen)
			{
				puu = puu->vasen;
				continue;
			}
			puu->vasen = uusi_solmu;
			uusi_solmu->isäntä = puu;
			break;
		}
		else
		{
			if (puu->oikea)
			{
				puu = puu->oikea;
				continue;
			}
			puu->oikea = uusi_solmu;
			uusi_solmu->isäntä = puu;
			break;
		}
	}
	return tasapainota(uusi_solmu);
}



inline static struct Solmu* hae_juuri(struct Solmu* solmu)
{
	while (solmu->isäntä) solmu = solmu->isäntä;
	return solmu;
}

inline static struct Solmu* poista(struct Solmu* solmu)
{
	assert(solmu->isäntä);
	struct Solmu* isäntä = solmu->isäntä;

	if (isäntä->vasen == solmu)
	{
		isäntä->vasen = NULL;
	} else {
		isäntä->oikea = NULL;
	}


	//struct Solmu* juuri = hae_juuri(solmu);


	if (solmu->vasen)
	{
		solmu->vasen->isäntä = NULL;
		lisää(isäntä, solmu->vasen);
	}
	if (solmu->oikea)
	{
		solmu->oikea->isäntä = NULL;
		lisää(isäntä, solmu->oikea);
	}


	return isäntä;
}


// TODO: Varaa suurepi määrä muistia kerralla valmiiksi josta jaetaan osuudet solmuille?
inline static struct Solmu* luo_solmu(long long avain)
{
	struct Solmu* solmu = malloc(sizeof(struct Solmu));
	solmu->avain = avain;
	solmu->vasen = NULL;
	solmu->oikea = NULL;
	solmu->isäntä = NULL;
	solmu->väri = PUNAINEN;
	solmu->data = NULL;

	return solmu;
}

void tuhoa_puu(struct Solmu* puu)
{
	if (puu->vasen) tuhoa_puu(puu->vasen);
	if (puu->oikea) tuhoa_puu(puu->oikea);
	free(puu);
}

inline static struct Solmu* lisää_juureen(struct Solmu** puu, long long avain)
{
	//if (hae(*puu, avain)) return;

	//KEHITYSTULOSTIN("Lisätään loikka %lld avaimella %lld\n", *laskin, avain);

	struct Solmu* solmu = luo_solmu(avain);
	struct Solmu* alin_palautettu = lisää(*puu, solmu);

	if (!alin_palautettu->isäntä)
	{
		alin_palautettu->väri = MUSTA;
		*puu = alin_palautettu;
		//KEHITYSTULOSTIN("Uusi puu (avain %lld) asetettu askeleellla %lld \n", alin_palautettu->avain,);
	}

	#if KEHITYSTILA
	assert(hae(*puu, avain));
	tarkista_puu(*puu);
	#endif
	KEHITYSTULOSTIN("\n");

	return solmu;
}

inline static struct Solmu* lisää_puuhun(struct Puu* puu, long long avain)
{
	struct Solmu* solmu = varaa_solmu(puu);
	solmu->vasen = solmu->oikea = NULL;
	solmu->avain = avain;
	puu->koko++;

	if (puu->juuri == NULL)
	{
		solmu->väri = MUSTA;
		puu->juuri = solmu;
		return solmu;
	}

	struct Solmu* alin = lisää(puu->juuri, solmu);
	if (!alin->isäntä)
	{
		puu->juuri = alin;
		alin->väri = MUSTA;
	}
	return solmu;
}

struct Solmu* poista_puusta(struct Puu* puu, struct Solmu* solmu)
{
	if (solmu == NULL)
	{
		printf("Ei poistettavaa solmua??\n");
		return NULL;
	}
	poista(solmu);
	solmu->vasen = NULL;
	solmu->oikea = NULL;
	solmu->isäntä = NULL;

	solmu->oikea = puu->vapautetut_solmut;
	puu->vapautetut_solmut = solmu;
	puu->koko--;

	return NULL;
}

int main(int lähetemäärä, char *lähetteet[])
{
	KEHITYSTULOSTIN("Aloitetaan\n\n");
	struct Puu* puu = luo_puu();

	lisää_puuhun(puu, 50);
	lisää_puuhun(puu, 65 );
	lisää_puuhun(puu, 62 );
	lisää_puuhun(puu, 70 );
	lisää_puuhun(puu, 62 );
	lisää_puuhun(puu, 75 );
	lisää_puuhun(puu, 5602 );
	lisää_puuhun(puu, 1024*1024*1024 );
	lisää_puuhun(puu, 55 );
	srand(time(NULL));


	printf("Lisätään %d solmua\n", MONTAKO);

	for (long long i = 100; i < 1024 * 1024 + 100; ++i)
	{
		//printf("%lld\n", i);
	//	long long avain = rand();
		//long long avain = i;
		//if (i % 8 == 0) avain *= -1;
		//lisää_puuhun(puu , i);
		lisää_puuhun(puu , rand());
		//poista_puusta(puu, hae(puu->juuri,i-55));
		#if KEHITYSTILA
		//assert(hae(puu->puu, avain));
		#endif
	}

	// https://users.pja.edu.pl/~jms/qnx/help/watcom/clibref/qnx/clock_gettime.html
	#define BILLION  1000000000L;
	struct timespec alku, loppu;
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &alku);
	for (size_t i = 0; i < 1024 * 1024; i++)
	{
		long long r = rand();
		sisältääkö(puu->juuri, r);
	}
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &loppu);
	double accum;

	accum = (loppu.tv_sec - alku.tv_sec) + (loppu.tv_nsec - alku.tv_nsec) / (float) BILLION;
	printf("%f\n", accum);

	//vapauta_puu(puu);
	printf("Haettu\n");

	//getchar();
	printf("Doed\n");
	//vapauta_puu(puu);
	//assert(jälkeläismäärä(puu->juuri) == MONTAKO);
	//struct Solmu* juuri = puu->juuri;
	//struct Solmu* puu = luo_solmu(50);
	//puu->väri = MUSTA;

	lisää_puuhun(puu, 10 );
	lisää_puuhun(puu, 60 );
	lisää_puuhun(puu, -55 );
	lisää_puuhun(puu, -1024 * 1024 * 1024 );
	//poista_puusta(puu, hae(puu->juuri, -55));
	//assert(hae(puu->juuri, -55) == NULL);

	//poista(puu->juuri->vasen);

	printf("Luotiin puu\n");
	printf("Korkeus: %lld\n", korkeus(puu->juuri));

	poista_puusta(puu, hae(puu->juuri, 65));


	assert(hae(puu->juuri, 1024 * 1024 * 1024));
	assert(hae(puu->juuri, 10));
	assert(hae(puu->juuri, -1024 * 1024 * 1024));

	hakuaika(puu->juuri, 10000000);
	hakuaika(puu->juuri, 1);
	hakuaika(puu->juuri, 5602);
	hakuaika(puu->juuri, 55);
	hakuaika(puu->juuri, 1024*1024*1024);

	assert(hae(puu->juuri, 65) == NULL);

	struct Solmu* suurin = hae_suurin(puu->juuri);
	printf("Suurin avain: %lld\n", suurin->avain);

	struct Solmu* pienin = hae_pienin(puu->juuri);
	printf("Pienin avain: %lld\n", pienin->avain);
	//struct Solmu *solmu = lisää_puuhun(puu, -1024  * 1024, );

	//tarkista_puu(puu->juuri);

	// printf("Jälkeläisiä %lld\n", jälkeläismäärä(puu->juuri));
	printf("Puun koko: %ld\n", puu->koko);


	//getchar();
	vapauta_puu(puu);

	return 0;
}

