#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define TULOSTA_KOKONAISLUKU(luku) printf("%d\n", luku)

int hae_alhaisin_indeksi(int* luvut, const int lukumaara, const unsigned int alku)
{
	unsigned int pienin_indeksi = alku;

	for (int i =  alku; i < lukumaara; i++)
	{
		if (luvut[i] < luvut[pienin_indeksi]) pienin_indeksi = i;
	}

	return pienin_indeksi;
}

int hae_alhaisin_arvo(int* luvut, const int lukumaara, const unsigned int alku)
{
	const int indeksi = hae_alhaisin_indeksi(luvut, lukumaara, alku);
	return luvut[indeksi];
}

void vaihda_arvot(int* luvut, const int indeksi1, const int indeksi2)
{
	const int arvo1 = luvut[indeksi1];
	const int arvo2 = luvut[indeksi2];

	luvut[indeksi1] = arvo2;
	luvut[indeksi2] = arvo1;
}

void jarjesta_luvut(int *luvut, const unsigned int lukumaara)
{
	for (int i = 0; i < lukumaara ; i++)
	{
		const unsigned int alhaisin_indeksi = hae_alhaisin_indeksi(luvut, lukumaara, i); 
		vaihda_arvot(luvut, i, alhaisin_indeksi);
	}
}


struct Haku
{
	int *luvut;
	unsigned short suunta; // 1 ylös, -1 alas
	unsigned int loikkakoko;
	unsigned short ohitusputki;

	unsigned int tavoiteltu_harava;
};

short int onko_arvoa(int *luvut, const unsigned int lukumaara, int haettu_arvo)
{
	const int arvo_keskella = luvut[lukumaara / 2];
	short int suunta = (haettu_arvo < arvo_keskella) ? -1 : 1; // -1 alas, 1 ylös
	
	int kursori = lukumaara / 2;

	int alin_mahdollinen = luvut[0];
	int ylin_mahdollinen = luvut[lukumaara-1];

	int ohituskerrat = 0;
	while(1) {
		if (haettava_arvo > ylin_mahdollinen) return -1;
		if (haettava_arvo < alin_mahdollinen) return -1;
		// alas
		//
		int ammunta_alue = kursori / (2 + ohituskerrat);
		if (ammunta_alue < 0

		
	}

}
int main()
{

	const int LUKUMAARA = 1024;
	int luvut[LUKUMAARA];
	
	time_t t;
	srand((unsigned) time(&t));

	for (int i = 0; i < LUKUMAARA; i++)
	{
		luvut[i] = rand() % 1024 * 32 + (rand() % 1024);
		//printf("%d\n", luvut[i]);
	}


	const int alhaisin_arvo = hae_alhaisin_arvo(luvut, LUKUMAARA, 0);
	const int alhaisin_indeksi = hae_alhaisin_indeksi(luvut, LUKUMAARA, 0);


	printf("alhaisin_arvo: %d\n", alhaisin_arvo);
	printf("alhaisin_indexi: %d\n", alhaisin_indeksi);

	jarjesta_luvut(luvut, LUKUMAARA);

	printf("%llu\n", (unsigned long long) '🎃');
  
	// TULOSTA_KOKONAISLUKU(luvut[1022]);
	// TULOSTA_KOKONAISLUKU(luvut[1023]);

	return 0;
}
