#include <stdio.h>
#include <wchar.h>
#include <locale.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include <time.h>

#include "s.h"


int main()
{
	//setlocale(LC_CTYPE, "");
   	setlocale(LC_ALL, "");
/*
	char* data[4096];

	Sivu sivu;
	sivu.kapasiteetti = 4096;
	sivu.data = data;
	sivu.käyttö = 0;

	Muisti muisti;
	muisti.kpl_sivut = 1;
	//malloc(&muisti.sivut, sizeof(void*));
	muisti.sivut = malloc(sizeof(void*));
	muisti.sivut[0] = &sivu;
	muisti.vapaat = 0;
*/
	Muisti muisti;
	alusta_muisti(&muisti);

	//ost_vapauta(&muisti, ost_varaa(&muisti, 11), 11);

	int* _ost = ost_varaa(&muisti, 100);
	ost_vapauta(&muisti, _ost, 100); 

	void* _asd = ost_varaa(&muisti, 50);
	ost_vapauta(&muisti, _asd, 50);

	ost_vapauta(&muisti, ost_varaa(&muisti, 50), 50);
	ost_vapauta(&muisti, ost_varaa(&muisti, 11), 11);

	Solmujono jono;
	alusta_solmujono(&jono, &muisti);
	//jono.muisti = &muisti;
	alusta_merkkisolmujono(&jono, L"kakit\0", -1);
	tulosta_solmujono(&jono);


	int arvo_kok = 666;
	Muuttuja *m_kok = lisää_muuttuja(&jono, NULL, MUUTTUJA_KOKONAISLUKU, &arvo_kok, L"luku\0");

	wchar_t* matti = L"Matti Meikäläinen?\0\0";
	size_t pituus = (wcslen(matti) + 1) * sizeof(wchar_t); 
	wchar_t* arvo_str = ost_varaa(&muisti, pituus); 
	//näytä_bitit(arvo_str, pituus);

	wprintf(L"AA:D\n");
	wcsncpy(arvo_str, matti, 18);
	//wcscpy(arvo_str, matti);
	wprintf(L"...AA:D\n");


	Muuttuja *m_str = lisää_muuttuja(&jono, NULL, MUUTTUJA_MERKKIJONO, arvo_str, L"nimi");
	lisää_merkkejä(&jono, L" ja vitut\0", -1);
	tulosta_solmujono(&jono);

	
	Solmujono jono2;
	alusta_solmujono(&jono2, &muisti);;
	alusta_merkkisolmujono(&jono2, L" ja kikit🐱🐱🐱🐱\0", -1);
	yhdistä_solmujonot(&jono, &jono2);
	

	lisää_merkkejä(&jono, L" ja vitut\0", -1);
	lisää_merkkejä(&jono, L"\n\n\tHEI!\n\tMinun nimeni on {{ nimi }} ja lempilukuni on {{ luku }}, hauska tutustua!\n\n\0\0", -1);

	SMuuttuja* muuttujasolmu = alusta_muuttujasolmu(&jono, NULL, m_kok);
	lisää_solmu(&jono, (Solmu*)muuttujasolmu, NULL);


	SMuuttuja* msolmu_str = alusta_muuttujasolmu(&jono, NULL, m_str); 
	lisää_solmu(&jono, (Solmu*) msolmu_str, NULL);

	lisää_merkkejä(&jono, L"[red]ja tässä punaista tekstiä[/red] VITUT\n\0", -1);

	arvo_kok = 6543210;

	clock_t aika_alku, aika_loppu;
	double cpu_aika;

	wprintf(L"häh..\n");

	aika_alku = clock();
	tulosta_solmujono(&jono);
	aika_loppu = clock();
	cpu_aika = ((double) (aika_loppu - aika_alku )) / CLOCKS_PER_SEC;

	wprintf(L"\nAikaa kului %fs\n", cpu_aika);

	for (int i = 0;  i < 1024 * 32; i++) 
	//for (int i = 1024 * 32; i >= 0; i--)
	{
		//wprintf(L"%d\n", i);
		//ost_varaa(&muisti, i);
		ost_vapauta(&muisti, ost_varaa(&muisti, i), i);
		//free(malloc(i));
		//malloc(i);
	}
	return 0;

/*
	Solmu *alku = hae(jono.alku, L"[red]", 5);
	assert(alku);
	alku = siirrä_indeksi(alku, 5);

	Solmu *loppu = hae(jono.alku, L"[/red]", 6);
	assert(loppu);
	siirrä_indeksi(loppu, 6);


	wprintf(L"\n'");
	while (alku && alku != loppu)
	{
		//putwchar(alku->arvo);
		tulosta_solmu(alku);
		alku = alku->seuraava;
	}
	wprintf(L"'\n");

	tulosta_solmujono(&jono);
	lisää_solmu_alkuun(&jono, alusta_merkkisolmu(NULL, '>' ));
	lisää_solmu_alkuun(&jono, alusta_merkkisolmu(NULL, '-' ));

	wchar_t wcstr[jono.pituus+1];

	memset(wcstr, 0, (jono.pituus + 1) * sizeof(wchar_t) );
	//wc_str(&jono, wcstr, jono.pituus);
	//wprintf(L"%ls", wcstr);

	wprintf(L"Häh?\n");

	for (size_t i = 0; i < 1024 * 1024; i++)
	{
		Solmu* solmu = alusta_merkkisolmu(NULL, (wchar_t) i);
		lisää_solmu(&jono, solmu, NULL);
	}

	tulosta_solmujono(&jono);
	tuhoa_solmujono(&jono);
	//tuhoa_solmujono(&jono2);
	//free(jono);
	//free(jono2);
	return 0;
	*/
}
