#include <time.h>
#include <locale.h>
#include <wchar.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <stdbool.h>
#include <assert.h>

#include "muisti.h"
#include "bitit.h"

 #define MAP_UNINITIALIZED 0x4000000    

static inline Sivu* alusta_sivu(SIVUKOKO kapasiteetti)
{
    assert(kapasiteetti <= SIVUN_KOKO_MAX);

    Sivu* sivu = (Sivu*) malloc(sizeof(Sivu));
    assert(sivu);
    //sivu->data = malloc(kapasiteetti);
    sivu->kapasiteetti = kapasiteetti;
    sivu->data =  mmap(NULL, sivu->kapasiteetti , PROT_READ | PROT_WRITE, MAP_PRIVATE |
        MAP_ANONYMOUS , 0, 0);
    
    if (sivu-> data == MAP_FAILED)
    {
        sivu->data =  mmap(NULL, kapasiteetti, PROT_READ | PROT_WRITE, MAP_PRIVATE |
            MAP_ANONYMOUS, 0, 0);
    }
    assert(sivu->data != MAP_FAILED);
    sivu->kapasiteetti--;
    sivu->käyttö = 0;

    #ifdef VIKATILA
    VIKATULOSTIN(L"Muisti mmapattu osoitteeseen %p\n", sivu->data);
    VIKATULOSTIN(L"Alustetaan uusi %d tavuinen sivu.\n", sivu->kapasiteetti);
    //memset(sivu->data, 0, kapasiteetti);
    assert(sivu->data);
    täytä_bitit((char*) sivu->data, 0, sivu->kapasiteetti);
    //näytä_bitit(sivu->data, kapasiteetti);
    #endif

    VIKATULOSTIN(L"..alustettu\n");

    //wprintf(L"Koko SIVUKOKO: %ld\n", sizeof(SIVUKOKO));
    //wprintf(L"Koko void*: %ld\n", sizeof(void*));
    //wprintf(L"Koko Vapaa: %ld\n", sizeof(Vapaa));

    return sivu;
}

Sivu* lisää_sivu(Muisti* muisti, SIVUKOKO sivun_koko)
{
    assert(muisti->kpl_sivut + 1 < SIVUT_KPL_MAX);
    assert(sivun_koko < SIVUN_KOKO_MAX);

    muisti->sivut = (Sivu**) realloc(muisti->sivut, (muisti->kpl_sivut + 1) * sizeof(Sivu*));
    assert(muisti->sivut);

    Sivu* uusi_sivu = alusta_sivu(sivun_koko);

    #ifdef VIKATILA 
    assert(on_tyhjä(uusi_sivu->data, sivun_koko));
    #endif

    muisti->sivut[(muisti->kpl_sivut)] = uusi_sivu; 
    muisti->kpl_sivut++;

    //wprintf(L"Sivu %d lisätty.\n", muisti->kpl_sivut);
    return uusi_sivu;
}

Muisti* alusta_muisti(Muisti* muisti)
{
    if (!muisti) muisti = (Muisti*) malloc(sizeof(Muisti));

    muisti->kpl_sivut = 0;
    muisti->sivut = NULL;
    lisää_sivu(muisti, SIVUN_KOKO_VAKIO); 
    muisti->vapaat = NULL;

    return muisti;
}


void ost_vapauta(Muisti* muisti, void* osoite, SIVUKOKO koko)
{
    VIKATULOSTIN(L"Vapautetaan %d tavuinen osoite %p\n", koko, osoite);
    if (koko < KOKO_VAPAA) 
    {
        VIKATULOSTIN(L"Hylätään osoite.");
        return;
    }
    #ifdef VIKATILA
    //memset(osoite, 0, koko);
    VIKATULOSTIN(L"Vapautetaan osoite %p %d tavulla.", osoite, koko);
    täytä_bitit(osoite, 0, koko);
    #endif

    // Tuskin oikeasti hyödyllistä etenkään tilanteissa joissa vapautetut ja pyydetyt osoitteet ovat yleensä saman kokoisia.
    if (muisti->vapaat && osoite + koko + 1 == muisti->vapaat)
    {
        VIKATULOSTIN(L"Yhdistetään peräkkäiset vapaat sektorit.\n");
        koko += muisti->vapaat->koko;
        muisti->vapaat->seuraava = ((Vapaa*)muisti->vapaat->seuraava)->seuraava;
        #ifdef VIKATIKA  
        täytä_bitit(muisti->vapaat->seuraava, 0, KOKO_VAPAA);
        #endif
    }

    ((Vapaa*)osoite)->koko = koko;
    ((Vapaa*)osoite)->seuraava = muisti->vapaat;
    #ifdef VIKATILA
    //näytä_bitit(osoite, koko);
    //wprintf(L"\n..\n");
    #endif
    muisti->vapaat = (Vapaa*) osoite;
    VIKATULOSTIN(L"..vapautettu.\n");
}

// TODO: Säilö vapautetut hajautustaulussa, puussa tai jotenkin muuten järjesteltynä?
static inline void* hae_vapautettu(Muisti* muisti, const SIVUKOKO koko_pyydetty)
{
    if (koko_pyydetty < KOKO_VAPAA)
    {
        return NULL;
    }

    Vapaa* vapaa = muisti->vapaat; 
    Vapaa* edellinen = NULL;

    void* palautettu_osoite = NULL;

    VIKATULOSTIN(L"Haetaan vapautettu (%d pyydetty)\n", koko_pyydetty);

    while (vapaa && !palautettu_osoite)
    {
        //VIKATULOSTIN(L"Katsotaan %p\n", vapaa);
        if(vapaa->koko >= koko_pyydetty)
        {
            VIKATULOSTIN(L"%d:n tavun vapaa lohko löytyi osoitteesta %p\n", vapaa->koko, vapaa);
            const SIVUKOKO annettu_häntä = vapaa->koko - koko_pyydetty;
            palautettu_osoite  = ((char*)vapaa) + annettu_häntä; 

            #ifdef VIKATILA
            // Ei huomioida ensimmäistä 10 tavua joiden ei kuulu olla tyhjiä.
            int erotus = (KOKO_VAPAA - vapaa->koko) +  koko_pyydetty;
            if (erotus < 0) erotus = 0;
            VIKATULOSTIN(L"Erotus: %d\n", erotus);
            if (!on_tyhjä(palautettu_osoite + erotus, koko_pyydetty - erotus))
                näytä_bitit(palautettu_osoite, koko_pyydetty);
            assert(on_tyhjä(palautettu_osoite + erotus, koko_pyydetty - erotus));
            #endif

            vapaa->koko -= koko_pyydetty;
            VIKATULOSTIN(L"Annettu %d tavua osoitteesta %p\n", koko_pyydetty, palautettu_osoite);

            //wprintf(L"aaaa: %d\n", (uintptr_t) palautettu_osoite - (uintptr_t) vapaa);
            assert((char*)vapaa + annettu_häntä == palautettu_osoite);

            if (vapaa->koko < KOKO_VAPAA)
            {
                VIKATULOSTIN(L"Jäänyt tynkä liian pieni uusiokäyttöön(%d), poistetaan jonosta.\n", vapaa->koko);
                if (edellinen)  edellinen->seuraava = vapaa->seuraava;
                else muisti->vapaat = vapaa->seuraava;
            }
            
        }
        else
        {
            //VIKATULOSTIN(L"Katsotaan seuraava\n");
            edellinen = vapaa;
            vapaa = vapaa->seuraava;
        }
    }

    #ifdef VIKATILA
    if (palautettu_osoite == NULL) {
        VIKATULOSTIN(L"Ei löytynyt sopivaa\n");
    } else {
        täytä_bitit(palautettu_osoite, 0, koko_pyydetty);
    }
    #endif

    return palautettu_osoite;
}


/*
 * Palauttaa annetusta ositteesta seuraavan tasauksella jaollisen osoitteen.
*/
inline static void* _sovita_osoite(const void* osoite, const SIVUKOKO tasaus)
{
    if (tasaus == 0) return (void*) osoite;
    const uint8_t mod = (uintptr_t) osoite % (uintptr_t) tasaus;
    if (mod == 0) return (void*) osoite;

    VIKATULOSTIN(L"\nSovitetaan osoite %p %d eteenpäin\n", osoite, mod);

    const void* palautettu =  (void*)((uintptr_t) osoite - mod + tasaus);

    VIKATULOSTIN(L"Sovitettu osoitteeseen %p (+%d)\n", palautettu, (uintptr_t) palautettu - (uintptr_t) osoite );

    return (void*) palautettu;
}

inline static void* _ost_varaa(Muisti* muisti, SIVUKOKO koko, SIVUKOKO tasaus)
{
    Sivu* sivu = muisti->sivut[muisti->kpl_sivut - 1];
    void* annettu_osoite = NULL;

    const SIVUKOKO koko_tarvittu = koko + tasaus;

    if (sivu->käyttö + koko_tarvittu > sivu->kapasiteetti)
    {
        VIKATULOSTIN(L"%d ei mahdu sivun loppuun, katsotaan löytyykö vapaata.\n", koko);
        annettu_osoite = hae_vapautettu(muisti, koko_tarvittu);

        if (annettu_osoite) {
            VIKATULOSTIN(L"Vapautettu löytyi.\n");
        }
        else 
        {
            VIKATULOSTIN(L"Lisätään sivu\n");
            sivu = lisää_sivu(muisti, (SIVUN_KOKO_VAKIO >= koko_tarvittu) ? SIVUN_KOKO_VAKIO : koko_tarvittu);
            VIKATULOSTIN(L"..sivu lisätty.\n");
        }
    } 

    if (annettu_osoite) {
        //VIKATULOSTIN(L"Annetaan käytetty osoite "); 
        annettu_osoite = _sovita_osoite(annettu_osoite, tasaus);
    } else {
        //VIKATULOSTIN(L"Annetaan neitseellinen osoite ");
        annettu_osoite = _sovita_osoite(((char*)sivu->data) + sivu->käyttö, tasaus);
        sivu->käyttö += koko_tarvittu;
    }
    //VIKATULOSTIN(L"%p\n", annettu_osoite);

    #ifdef VIKATILA
    if (!on_tyhjä(annettu_osoite, koko))
    {
        wprintf(L"Osoite %p ei ole tyhjä!\n", annettu_osoite);
        näytä_bitit(annettu_osoite, koko);
        wprintf(L"\n");
        exit(1);
    }
    //memset(annettu_osoite, 0, koko);
    täytä_bitit(annettu_osoite, 1, koko);
    #endif

    //wprintf(L"Käyttämätön osoite annettu\n");
    return annettu_osoite;
}

inline void* ost_varaa(Muisti* muisti, SIVUKOKO koko) 
{
    return _ost_varaa(muisti, koko, 8);
}

inline void* ost_varaa_tasauksella(Muisti *muisti, SIVUKOKO koko, SIVUKOKO tasaus)
{
    if (tasaus == 0) tasaus = sizeof(koko);
    //wprintf(L"tasaus %d koko %d\n", tasaus, koko);
    return _ost_varaa(muisti, koko, tasaus);
    //return _ost_varaa(muisti, koko, 0);
}