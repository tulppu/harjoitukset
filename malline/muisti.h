#ifndef MUISTI_H_
#define MUISTI_H_


#include <stdint.h>
#include <time.h>

typedef uint16_t    KOKO_VAPAA;
typedef uint16_t    SIVUKOKO;

#define SIVUN_KOKO_VAKIO 4096 
//#define SIVUN_KOKO_MAX 65535
#define SIVUN_KOKO_MAX UINT16_MAX 
#define SIVUT_KPL_MAX UINT32_MAX


#define VIKATILA 1

#ifdef VIKATILA
    #define VIKATULOSTIN(...) wprintf(__VA_ARGS__);
#else
    #define VIKATULOSTIN(...)
#endif

typedef struct Vapaa
{
    void*       seuraava;
    KOKO_VAPAA  koko;
}  __attribute__((__packed__, aligned(2))) Vapaa;
#define KOKO_VAPAA 10


typedef struct Sivu
{
    char*       data;
    SIVUKOKO    kapasiteetti;
    SIVUKOKO    käyttö;
} Sivu;

typedef struct Muisti
{
    Sivu**      sivut;
    uint32_t    kpl_sivut;
    Vapaa*      vapaat;
} Muisti;

//static Sivu* alusta_sivu(SIVUKOKO kapasiteetti);
Sivu*   lisää_sivu(Muisti* muisti, SIVUKOKO sivun_koko);
Muisti* alusta_muisti(Muisti* muisti);
//static void* hae_vapautettu(Muisti* muisti, size_t koko);
void*   ost_varaa(Muisti* muisti, SIVUKOKO koko);
void*   ost_varaa_tasauksella(Muisti* muisti, SIVUKOKO koko, SIVUKOKO täyte);
void    ost_vapauta(Muisti* muisti, void* osoite, SIVUKOKO koko);

#endif