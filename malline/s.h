
#ifndef S_H_ 
#define S_H_

#include <time.h>
#include "muisti.h"

typedef enum  {
	SOLMU_MERKKI 		= 0,
	SOLMU_MUUTTUJA		= 1,
} Solmutyyppi;

typedef enum {
	MUUTTUJA_MERKKIJONO,
	MUUTTUJA_KOKONAISLUKU,
	MUUTTUJA_DESIMAALI,
	MUUTTUJA_TOTUUSARVO
} Muuttujatyyppi;

typedef struct Muuttuja
{
	Muuttujatyyppi tyyppi;
	void* arvo;
	wchar_t* nimi;
} Muuttuja;


// Solmut
typedef struct Solmu
{
	Solmutyyppi tyyppi;
	void* seuraava;
	void* edellinen;
	void* arvo;	
} Solmu;

typedef struct SMerkki 
{
	Solmutyyppi tyyppi;
	void* seuraava;
	void* edellinen;
	wchar_t arvo;	
} SMerkki;

typedef struct SMuuttuja
{
	Solmutyyppi styyppi;
	void* seuraava;
	void* edellinen;
	Muuttuja* arvo;
} SMuuttuja;

// Jono
typedef struct Solmujono 
{
	size_t pituus;
	Solmu* alku;
	Solmu* perä;

	Solmu* vapaat_solmut;

	Muisti* muisti;

	Muuttuja **muuttujat;
	unsigned char kpl_muuttujat;
} Solmujono;


void alusta_solmujono(Solmujono*, Muisti* muisti);

Solmu* siirrä_indeksi(Solmu* solmu, short i);
Solmu* alusta_solmu(Solmujono*, Solmu* solmu, Solmutyyppi tyyppi);
Solmu* alusta_merkkisolmu(Solmujono*, SMerkki *solmu, wchar_t arvo);
Solmu* siirrä_indeksi(Solmu* solmu, short i);
Solmu* hae(Solmu* solmu, wchar_t* merkit, size_t pituus);
void poista_solmu(Solmujono*, Solmu*);
void parita_solmut(Solmu* solmu1, Solmu* solmu2);
bool onko_merkki(Solmu* solmu, wchar_t merkki);

Muuttuja* lisää_muuttuja(Solmujono* jono, Muuttuja *muuttuja, Muuttujatyyppi tyyppi, void* arvo, wchar_t* nimi);
Muuttuja* hae_muuttuja(Solmujono* jono, wchar_t* nimi);

SMuuttuja* alusta_muuttujasolmu(Solmujono* jono, SMuuttuja* solmu, Muuttuja* muuttuja);


int alusta_merkkisolmujono(Solmujono* solmujono, wchar_t* merkit, size_t pituus);
void lisää_solmu(Solmujono* solmujono, Solmu* solmu, Solmu* edellinen);
Solmujono* yhdistä_solmujonot(Solmujono* jono1, Solmujono* jono2);
void lisää_solmu_alkuun(Solmujono* Solmujono, Solmu* solmu);
void lisää_merkkejä(Solmujono* Solmujono, wchar_t* merkit, size_t pituus);
void tuhoa_solmujono(Solmujono* Solmujono);
void tulosta_solmujono(Solmujono* Solmujono);


void lisää_merkkejä(Solmujono* Solmujono, wchar_t* merkit, size_t pituus);

void wc_str(Solmu* alku, Solmu* loppu, wchar_t* kohde);
void poista_solmu(Solmujono* jono, Solmu* solmu);
void tulosta_solmujono(Solmujono*);
void tulosta_solmu(Solmu* solmu);
void vpoistu(wchar_t* viesti);


#endif