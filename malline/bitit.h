
#include <time.h>
#include <stdint.h>

#ifndef BITIT_H_
#define BITIT_H_

#define ASETA_BITTI(tavu, mones)    ((tavu) |= (1<<(mones)))
#define POISTA_BITTI(tavu, n)   ((tavu) &= ~(1 << (n)))
#define LUE_BITTI(tavu, n)      ((tavu) &   (1 << (n)))


void            täytä_bitit(void* ost, int millä, size_t koko);
void            näytä_bitit(void* ost, size_t koko);
char            on_tyhjä(void* ost, size_t koko);
char            on_täysi(void* ost, size_t koko);

#endif