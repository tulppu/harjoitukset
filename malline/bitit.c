#include <stdint.h>
#include <time.h>
#include <stdbool.h>
#include <assert.h>
#include <wchar.h>

#include "bitit.h"

void täytä_bitit(void* ost, int millä, size_t koko)
{
    assert(millä == 0 || millä == 1);
    for (size_t i = 0; i < koko; i++)
    {
        // wprintf(L"%d\n", i);
        uint16_t* tavu = (uint16_t*)(ost + i);
        for (int y = 0; y < 8; y++)
        {
            if (millä == 1)
                ASETA_BITTI(*tavu, (int)y);
            else if (millä == 0)
                POISTA_BITTI(*tavu, (int)y);
        }
    }
}

void näytä_tavu(uint8_t* tavu)
{

    if ( (uintptr_t) tavu % 8 == 0) 
        putwchar(L'\n');
    
    wprintf(L" %2d. | ", 1 + ((uintptr_t) tavu  % 8));
    wprintf(L"%p ", tavu);
    wprintf(L" [ ");
    for (int y = 0; y < 8; y++)
    {
        const bool onko_päällä = LUE_BITTI(*tavu, (int) y);
        if (onko_päällä)
        {
            wprintf(L"\033[1;31m%u\033[0m", 1);
        }
        else
        {
            //wprintf(L"%u", 0);
            wprintf(L"\033[1;37m%u\033[0m", 0);
        }
    }
    putwchar(L' ');
    wprintf(L"] ");
    wprintf(L" %02X | %01c | %02d", *tavu, (unsigned char) *tavu, *tavu);
    putwchar(L'\n');
}

void näytä_bitit(void* ost, size_t koko)
{
    wprintf(L"Sisältö osoitteessa @ %p (%d tavua):\n\n", ost, koko);
    for (size_t i = 0; i < koko; i++)
    {
        uint8_t *tavu = (uint8_t*)(ost + i);
        näytä_tavu(tavu);
    }
    wprintf(L"\n ");
}

char on_tyhjä(void* ost, size_t koko)
{
    for (size_t i = 0; i < koko; i++)
    {
        uint8_t* tavu = (uint8_t*)(ost+1);
        for (uint8_t y = 0; y < 8; y++)
        {
            unsigned int bitti = (*tavu &  (1<<(y))); 
            if (bitti != 0) return 0;
        }
    }
    return 1;
}

char on_täysi(void* ost, size_t koko)
{
    for (size_t i = 0; i < koko; i++)
    {
        uint8_t* tavu = (uint8_t*)(ost+1);
        for (uint8_t y = 0; y < 8; y++)
        {
            unsigned int bitti = (*tavu &  (1<<(y))); 
            if (bitti == 0) 
                return 0;
        }
    }
    return 1;
}