#include <stdio.h>
#include <wchar.h>
#include <locale.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include <time.h>

#include "s.h"
#include "muisti.h"

void vpoistu(wchar_t* viesti)
{
	if (viesti) {
		wprintf(viesti);
	}
	perror("Virhe:" );
	exit(1);
}

void alusta_solmujono(Solmujono* jono, Muisti* muisti)
{
	if (muisti == NULL)
	{
		muisti = (Muisti*) malloc(sizeof(Muisti));
		alusta_muisti(jono->muisti);
	}
	jono->muisti = muisti;
}

// Puskuri voi ylitäyttyä.
void täytä_puskuria(Solmu *solmu, wchar_t* puskuri, size_t* pituus)
{
	Muuttuja* muuttuja =  NULL;
	size_t tmp_pituus = 0;
	wchar_t tmp_str[20] = {0};
	int luku = 0;

	switch(solmu->tyyppi)
	{
		case SOLMU_MERKKI:
			puskuri[*pituus] = ((SMerkki*)solmu)->arvo;
			++*pituus;
			return;
		case SOLMU_MUUTTUJA:
			muuttuja = ((SMuuttuja*)solmu)->arvo;
			switch(muuttuja->tyyppi )
			{
				case MUUTTUJA_MERKKIJONO:
					tmp_pituus = wcslen(muuttuja->arvo);
					for (size_t i = 0; i < tmp_pituus; i++)
						puskuri[(*pituus)+i] = ((wchar_t*) muuttuja->arvo)[i];

					*pituus += tmp_pituus;
					return;
				
				case MUUTTUJA_KOKONAISLUKU:
					luku = *((int*)muuttuja->arvo);

					char i = 0;
					while (luku) {
						const int mod = luku % 10;
						tmp_str[(int)i++] = mod + L'0';
						luku /= 10;
					}

					for (char y = 0; y < i; y++)
						puskuri[(*pituus)+y] = tmp_str[(int)y];

					*pituus += i;

					return;

				case MUUTTUJA_TOTUUSARVO:
				case MUUTTUJA_DESIMAALI:
				default:
					break;;
			}
			break;
		default:
			wprintf(L"%d\n", solmu->tyyppi);
			vpoistu(L"Outo tyyppi!\n");
			break;
	}
}

void tulosta_solmu(Solmu* solmu)
{
	Muuttuja* muuttuja = ((SMuuttuja*)solmu)->arvo;
	switch(solmu->tyyppi)
	{
		case SOLMU_MERKKI:
			putwchar(((SMerkki* )solmu)->arvo);
			break;
		case SOLMU_MUUTTUJA:
			switch(muuttuja->tyyppi )
			{
				case MUUTTUJA_MERKKIJONO:
					wprintf(L"%ls", muuttuja->arvo);
					return;
				
				case MUUTTUJA_KOKONAISLUKU:
					wprintf(L"%d", *(int*)muuttuja->arvo);
					return;

				case MUUTTUJA_TOTUUSARVO:
				case MUUTTUJA_DESIMAALI:
				default:
					break;;
			}
			break;
		default:
			wprintf(L"%d\n", solmu->tyyppi);
			vpoistu(L"Ihme tyyppi!\n");
			break;
	}
}


size_t solmukoko(Solmutyyppi tyyppi)
{
	switch (tyyppi)
	{
		case SOLMU_MERKKI:
			return sizeof(SMerkki);
		case SOLMU_MUUTTUJA:
			return sizeof(SMuuttuja);
		default:
			vpoistu(L"Viallinen solmutyyppi!");
	}
	return 0;
}

Solmu* alusta_solmu(Solmujono* jono, Solmu* solmu, Solmutyyppi tyyppi)
{
	if (!solmu) 
	{
		//solmu = (Solmu*) ost_varaa(jono->muisti, solmukoko(tyyppi));
		solmu = (Solmu*) ost_varaa_tasauksella(jono->muisti, solmukoko(tyyppi), sizeof(Solmu*));
	}
	assert(tyyppi == SOLMU_MERKKI  || tyyppi == SOLMU_MUUTTUJA);

	solmu->seuraava	= solmu->edellinen = NULL;
	solmu->tyyppi = tyyppi;

	return solmu;
}

SMuuttuja* alusta_muuttujasolmu(Solmujono *jono, SMuuttuja* solmu, Muuttuja* muuttuja) 
{
	solmu = (SMuuttuja*) alusta_solmu(jono, (Solmu*) solmu, SOLMU_MUUTTUJA);
	solmu->arvo = muuttuja;

	return solmu;
}

Solmu* alusta_merkkisolmu(Solmujono *jono, SMerkki *solmu, wchar_t arvo)
{
	if (solmu == NULL)
		solmu = (SMerkki*) ost_varaa_tasauksella(jono->muisti, sizeof(SMerkki), sizeof(void*));

	solmu->arvo = arvo;
	solmu->seuraava = NULL;
	solmu->tyyppi =  SOLMU_MERKKI;
	solmu->edellinen = NULL;

	return (Solmu*) solmu;
}

void lisää_merkkejä(Solmujono* jono, wchar_t* merkit, size_t pituus)
{
	if (pituus == -1) pituus = wcslen(merkit);
	for (size_t i = 0; i < pituus; i++)
	{
		Solmu* solmu = alusta_merkkisolmu(jono, NULL, merkit[i]);
		lisää_solmu(jono, solmu, NULL);
	}
}

void poista_solmu(Solmujono* jono, Solmu* solmu)
{
	if (solmu == jono->alku)
	{
		jono->alku = solmu->seuraava;
		jono->alku->edellinen = NULL;

		ost_vapauta(jono->muisti, solmu, solmukoko(solmu->tyyppi));
		return;
	}

	if (solmu == jono->perä)
	{
		jono->perä = solmu->edellinen;
		jono->perä->seuraava = NULL;

		ost_vapauta(jono->muisti, solmu, solmukoko(solmu->tyyppi));
		return;
	}
	((Solmu*)solmu->edellinen)->seuraava = solmu->seuraava;
	((Solmu*)solmu->seuraava)->edellinen = solmu->edellinen;
	//ost_vapauta(jono->muisti, solmu, solmukoko(solmu->tyyppi));
}	

Muuttuja* lisää_muuttuja(Solmujono* jono, Muuttuja *muuttuja, Muuttujatyyppi tyyppi, void* arvo, wchar_t* nimi)
{
	//if (!muuttuja) muuttuja = (Muuttuja*) malloc(sizeof(Muuttuja));
	if (!muuttuja) muuttuja = (Muuttuja*) ost_varaa_tasauksella(jono->muisti, sizeof(Muuttuja), 8);

	muuttuja->arvo = arvo;
	muuttuja->tyyppi = tyyppi;
	muuttuja->nimi = malloc((wcslen(nimi) + 1) * sizeof(wchar_t));
	muuttuja->nimi[wcslen(nimi)-1] = L'\0';

	wcscpy(muuttuja->nimi, nimi);

	// TODO: Varaa muisti järkevämmin jos tulee tarpeeseen, tuskin tulee.
	jono->muuttujat = realloc(jono->muuttujat, jono->kpl_muuttujat + 1 * sizeof(Muuttuja));
	if (!jono->muuttujat)
		vpoistu(L"Muistiongelma");

	jono->muuttujat[jono->kpl_muuttujat] = muuttuja;
	jono->kpl_muuttujat++;

	return muuttuja;
}

Muuttuja* hae_muuttuja(Solmujono* jono, wchar_t* nimi)
{
	// TODO: Pidä taulukko järjestyksessä tai säilö hajautustauluna ja hae tehokkaammin jos tulee tarpeen.
	for (unsigned char i = 0; i < jono->kpl_muuttujat; i++)
	{
		if (wcscmp(jono->muuttujat[i]->nimi, nimi) == 0)
		{
			return jono->muuttujat[i];
		}
	}
	return NULL;
}


void wc_str(Solmu* alku, Solmu* loppu, wchar_t* kohde)
{
	Solmu* solmu = alku;
	unsigned long i = 0;
	while(solmu && solmu != loppu->seuraava)
	{
		if (solmu->tyyppi == SOLMU_MERKKI)
		{
			kohde[i] = (wchar_t) ((SMerkki*)solmu)->arvo;
			i++;
		}
		solmu = solmu->seuraava;
	}
}

int alusta_merkkisolmujono(Solmujono* solmujono, wchar_t* merkit, size_t pituus)
{
	if (pituus == -1) pituus = wcslen(merkit);

	solmujono->pituus = pituus;
	solmujono->alku = solmujono->perä = NULL;

	solmujono->muuttujat = NULL;
	solmujono->kpl_muuttujat = 0;

	for (size_t i = 0; i < pituus; i++)
	{
		Solmu* solmu = alusta_merkkisolmu(solmujono, NULL, merkit[i]);
		if (i == 0)
		{
			solmujono->alku = solmujono->perä = solmu;
		}
		else
		{
			solmujono->perä->seuraava = solmu;
			solmu->edellinen = solmujono->perä;
		}
		solmujono->perä = solmu;
	}

	return 1;
}


void tuhoa_solmujono(Solmujono* jono)
{
	Solmu *solmu = jono->alku;
	while (solmu) {
		if (solmu->edellinen) free((Solmu*)solmu->edellinen);
		solmu = solmu->seuraava;
	}
	//free(jono->perä);
	//free(Solmujono);
}


Solmujono* yhdistä_solmujonot(Solmujono* jono1, Solmujono* jono2)
{
	jono1->perä->seuraava = jono2->alku;
	jono2->alku->edellinen = jono1->perä;
	jono1->perä = jono2->perä;
	jono1->pituus += jono2->pituus;

	return jono1;
}


void lisää_solmu_alkuun(Solmujono* Solmujono, Solmu* solmu)
{
	solmu->seuraava = Solmujono->alku;
	Solmujono->alku->edellinen = solmu;
	Solmujono->alku = solmu;
	Solmujono->pituus++;
}

void parita_solmut(Solmu* solmu1, Solmu* solmu2)
{
	solmu1->seuraava = solmu2;
	solmu2->edellinen = solmu1;
}

bool onko_merkki(Solmu* solmu, wchar_t merkki)
{
	return solmu && solmu->tyyppi == SOLMU_MERKKI && ((SMerkki*)solmu)->arvo == merkki;
}

void lisää_solmu(Solmujono* solmujono, Solmu* solmu, Solmu* edellinen)
{
	if (edellinen == NULL) edellinen = solmujono->perä;

	solmu->edellinen = edellinen;
	solmu->seuraava = edellinen->seuraava;
	edellinen->seuraava = solmu;

	if (edellinen == solmujono->alku)
	{
		solmujono->alku = solmu;
	}
	if (edellinen == solmujono->perä)
	{
		solmujono->perä = solmu;
	}
	solmujono->pituus++;

	if (onko_merkki(solmu, L'}') && onko_merkki(solmu->edellinen, L'}'))
	{

		Solmu* alku = ((Solmu*) solmu->edellinen)->edellinen;
		if (!onko_merkki(alku, ' ')) return;

		Solmu* tmp = alku->edellinen;

		for (char i = 0; i < 24 && tmp; i++)
		{
			if (onko_merkki(tmp, L' '))
			{

				if (onko_merkki(tmp->edellinen, L'{')
					&& onko_merkki(((Solmu*)tmp->edellinen)->edellinen, L'{'))
				{
					Solmu* nimi_alku = tmp->seuraava;
					Solmu* nimi_loppu = alku->edellinen;

					wchar_t muuttujan_nimi[i+1];
					memset(muuttujan_nimi, L'\0', (i + 1) * sizeof(wchar_t));
					wc_str(nimi_alku, nimi_loppu, muuttujan_nimi);

					//wprintf(L"\nmuuttuja: '%ls'\n", muuttujan_nimi);

					Muuttuja* muuttuja = hae_muuttuja(solmujono, muuttujan_nimi);
					if (!muuttuja)
					{
						return;
						vpoistu(L"Ei muuttujaa!");
					}

					SMuuttuja* msolmu = alusta_muuttujasolmu(solmujono, NULL, muuttuja); 
					//Solmu* alku = ((Solmu*)tmp->edellinen)->edellinen;
					Solmu* alku = siirrä_indeksi(tmp, -3); 
					

					parita_solmut( (Solmu*) alku, (Solmu*) msolmu);
					if (solmu->seuraava) {
						parita_solmut((Solmu*) msolmu, (Solmu*) solmu->seuraava);
					} else {
						solmujono->perä = (void*) msolmu;
					}
					// TODO: Vapauta solmut välistä
					
				}
				else
				{
					//wprintf(L"\nLOPPU!\n");
					//exit(0);
					break;
				}
			}

			tmp = tmp->edellinen;
		}


	}

}

void tulosta_solmujono(Solmujono* jono)
{
	Solmu* solmu = jono->alku;
	wchar_t puskuri[1024];
	size_t pituus = 0;

	while (solmu)
	{
		//itulosta_solmu(solmu);
		täytä_puskuria(solmu, puskuri, &pituus);
		solmu = solmu->seuraava;
		//putwchar(solmu->arvo);
		//putwchar( (wchar_t)(pituus));
	}
	puskuri[pituus] = L'\0';
	wprintf(L"%ls", puskuri);
}

Solmu* siirrä_indeksi(Solmu* solmu, short i)
{
	bool suunta_eteen = i > 0;
	if (i < 0) i*=-1;
	for (size_t y = 0; y < i && solmu; y++)
	{
		solmu = (suunta_eteen) ? solmu->seuraava : solmu->edellinen;
	}
	return solmu;	
}

Solmu* hae(Solmu* solmu, wchar_t* merkit, size_t pituus)
{
	Solmu* alku = NULL;
	size_t putki = 0;
	while(solmu)
	{
		if (solmu->tyyppi != SOLMU_MERKKI)
		{
			solmu = solmu->seuraava;
			continue;
		}
		if (merkit[putki] == ((SMerkki*) solmu)->arvo)
		{
			if (putki == 0) alku = solmu;
			putki++;

			if (putki == pituus - 1)
				break;
		}
		else 
		{
			putki = 0;
			alku = NULL;
		}
		solmu = solmu->seuraava;
	}
	return alku;
}

/*
void wc_str(Solmujono *jono, wchar_t* kohde, size_t pituus)
{
	Solmu* solmu = jono->alku;
	for (size_t i = 0; i < pituus; i++)
	{
		kohde[i] = solmu->arvo;
		solmu = solmu->seuraava;
	}
	kohde[pituus] = L'\0';
}*/

