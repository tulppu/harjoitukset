CC=gcc -Wall -std=c11 -O2
CFLAGS=-I.

merkkijono: merkkijono.c
		$(CC) -o merkkijonot merkkijono.c

puu: puu.c
		$(CC) -o puu puu.c

cppmap: cppmap.cpp 
		g++ -O3 -o cppmap cppmap.cpp 
